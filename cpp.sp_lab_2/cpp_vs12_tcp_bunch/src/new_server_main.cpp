#define _CRT_SECURE_NO_WARNINGS

#include "common.h"
#include "server.h"
#include <WinSock2.h>
#include <cstdio>

int main(){
	int iResult = EXIT_FAILURE;
	do{
		puts("<server> is starting up.");
		if(!NTcp::CServer::set_up())
			break;
		else{
			puts("<server> initialization.");
			NTcp::CServer& server = NTcp::CServer::get_instance();
			if(!server.is_good_instance())
				break;
			puts("<server> starts working.");
			
			server.start_thread_of_acception_new_clients();
			do{
				char sBuffer[PLAIN_STR];
				gets(sBuffer);
				server.send_all(sBuffer);

				server.decept_obsolete_clients();
				if(server.is_empty())
					break;
			}while(true);
			iResult = EXIT_SUCCESS;
		}
	}while(false);
	WSACleanup();
	return iResult;	 
}
