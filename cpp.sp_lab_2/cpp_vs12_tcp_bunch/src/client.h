#pragma once
#include <WinSock2.h>

namespace NTcp{
	class CClient{
	public:
		SOCKET m_connection;
		HANDLE m_in;
		HANDLE m_out;

		bool m_is_connected;

	public:
		CClient(SOCKET);
		int const send_message(char const*);
		int const recieve_message(char*);

		bool const is_obsolete()const;
	};
};