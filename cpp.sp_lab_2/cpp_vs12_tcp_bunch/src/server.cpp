#include "common.h"
#include "server.h"
#include "client.h"

#include <WinSock2.h>
#pragma comment(lib,"ws2_32.lib")

using namespace NTcp;

bool const CServer::set_up(){
	static bool bWsaStartUpSuccess = !WSAStartup(MAKEWORD(2,2), new WSADATA);
	return bWsaStartUpSuccess;
}
CServer& CServer::get_instance(){
	static CServer instance;
	return instance;
}
CServer::CServer(){
	m_is_good = false;
	
	if((m_listener = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) == INVALID_SOCKET)
		return;

	m_soadin.sin_family = AF_INET;
	m_soadin.sin_addr.s_addr = INADDR_ANY;
	m_soadin.sin_port = htons(1488);
	
	if(bind(m_listener, (PSOCKADDR)&m_soadin, sizeof(m_soadin)) == SOCKET_ERROR)
		return;

	if(listen(m_listener, SOMAXCONN))
		return;

	m_is_good = true;
}

bool const CServer::is_good_instance()const{
	return m_is_good;
}

bool const CServer::is_empty()const{
	return m_clients.empty();
}

DWORD WINAPI ThreadOfAcception(PVOID){
	while(true)
	{
		SOCKET new_client = accept(CServer::get_instance().m_listener, NULL, NULL);	
		if(new_client != INVALID_SOCKET)
			CServer::get_instance().m_clients.push_back(new CClient(new_client));
		Sleep(100);
	}
}
bool const CServer::start_thread_of_acception_new_clients(){
	static HANDLE h = CreateThread(NULL, 0, ThreadOfAcception, 0, 0, new DWORD);
	return true;
}

bool const CServer::decept_obsolete_clients(){
	for(CContainer::const_iterator it=m_clients.begin(); it<m_clients.end(); ++it){
		if((**it).is_obsolete()){
			delete *it;
			m_clients.erase(it);
		}
	}
	return true;
}

bool const CServer::send_all(char const* c_sMessage){
	char c='0';
	for(CContainer::iterator it=m_clients.begin(); it!=m_clients.end(); ++it){
		printf("Sending %c.\n", c++);
		(**it).send_message(c_sMessage);
	}
	return true;
}

