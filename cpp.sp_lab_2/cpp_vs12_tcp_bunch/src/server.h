#pragma once
#include "client.h"
#include <WinSock2.h>
#include <vector>

namespace NTcp{
	class CServer{
	public:
		typedef std::vector<CClient*> CContainer;
	public:
		SOCKET m_listener;
		SOCKADDR_IN m_soadin;
		CContainer m_clients;
		bool m_is_good;
	private:
		CServer();
	public:
		static bool const set_up();
		static CServer&	  get_instance();

		bool const is_good_instance() const;
		bool const is_empty() const;

		bool const start_thread_of_acception_new_clients();
		bool const decept_obsolete_clients();

		bool const send_all(char const*);
	};
}