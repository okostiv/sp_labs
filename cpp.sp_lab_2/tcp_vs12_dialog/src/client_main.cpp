#include "tcp_common.h"
#include "send_recieve_thread_functins.h"

#include <iostream>
#include <winsock2.h>
#pragma comment(lib,"ws2_32.lib")

int main(){
	do{
		// Initialise Winsock
		WSADATA wsad;
		if(WSAStartup(MAKEWORD(2,2),&wsad)){
			std::cout<<"Winsock error - Winsock initialization failed\r\n";
			break;
		}
		
		// Create our socket
		SOCKET Socket=socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
		if(Socket==INVALID_SOCKET){
			std::cout<<"Winsock error - Socket creation Failed!\r\n";
			break;
		}

		// Resolve IP address for hostname
		struct hostent *host;
		if(!(host=gethostbyname("localhost"))){
			std::cout<<"Failed to resolve hostname.\r\n";
			break;
		}

		// Setup our socket address structure
		SOCKADDR_IN SockAddr;
		SockAddr.sin_port=htons(1488);
		SockAddr.sin_family=AF_INET;
		SockAddr.sin_addr.s_addr=*((unsigned long*)host->h_addr);

		// Attempt to connect to server
		if(connect(Socket,(SOCKADDR*)(&SockAddr),sizeof(SockAddr))){
			std::cout<<"Failed to establish connection with server\r\n";
			break;
		}else
			std::cout << "Connected successfully." << std::endl;
		
		// dialog
		HANDLE hSendThread = CreateThread(NULL, 0, SendThread, &Socket, 0, new DWORD);
		HANDLE hRecieveThread = CreateThread(NULL, 0, RecieveThread, &Socket, 0, new DWORD);

		WaitForMultipleObjects(1, &hSendThread, TRUE, INFINITE);

		// Shutdown our socket
		shutdown(Socket,SD_SEND);

		// Close our socket entirely
		closesocket(Socket);
	}while(FALSE);
	// Cleanup Winsock
	WSACleanup();
	system("PAUSE");
	return 0;
}