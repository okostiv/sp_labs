#include "tcp_common.h"
#include "send_recieve_thread_functins.h"

#include <iostream>
#include <winsock2.h>
#pragma comment(lib,"ws2_32.lib")

int main(){ 
	do{
		if(WSAStartup(MAKEWORD(2,2), new WSADATA)){
			std::cout<<"WSA Initialization failed!\r\n";
			break;
		}
		
		SOCKET Socket=socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
		if(Socket==INVALID_SOCKET){
			std::cout<<"Socket creation failed.\r\n";
			break;
		}																						    
		
		SOCKADDR_IN serverInf;
		serverInf.sin_family=AF_INET;
		serverInf.sin_addr.s_addr=INADDR_ANY;
		serverInf.sin_port=htons(1488);

		if(bind(Socket,(SOCKADDR*)(&serverInf),sizeof(serverInf))==SOCKET_ERROR){
			std::cout<<"Unable to bind socket!\r\n";
			break;
		}

		listen(Socket,1);

		SOCKET TempSock=SOCKET_ERROR;
		//while(TempSock==SOCKET_ERROR)
		{
			std::cout<<"Waiting for incoming connections...\n";
			TempSock=accept(Socket,NULL,NULL);
		}
		Socket=TempSock;

		std::cout<<"Client connected!\n\n";

		HANDLE hSendThread = CreateThread(NULL, 0, SendThread, &Socket, 0, new DWORD);
		HANDLE hRecieveThread = CreateThread(NULL, 0, RecieveThread, &Socket, 0, new DWORD);

		WaitForMultipleObjects(1, &hSendThread, TRUE, INFINITE);
		// Shutdown our socket
		shutdown(Socket,SD_SEND);

		// Close our socket entirely
		closesocket(Socket);
	}while(FALSE);
	// Cleanup Winsock
	WSACleanup();
	system("PAUSE");
	return 0;
}