#define _CRT_SECURE_NO_WARNINGS
#include <winsock2.h>
#include <Ws2tcpip.h>
#include <stdio.h>

// Link with ws2_32.lib
#pragma comment(lib, "Ws2_32.lib")

int main(){
	int iResult = EXIT_FAILURE;
	do{
		if (WSAStartup(MAKEWORD(2, 2), new WSADATA)) {
			printf("WSAStartup failed with error %d\n", WSAGetLastError());
		    break;
		}
		SOCKET RecvSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
		if (RecvSocket == INVALID_SOCKET) {
		    printf("socket failed with error %d\n", WSAGetLastError());
		    break;
		}

		SOCKADDR_IN RecvAddr = {0};
		USHORT Port = 27015;
		RecvAddr.sin_family = AF_INET;
		RecvAddr.sin_port = htons(Port);
		RecvAddr.sin_addr.s_addr = htonl(INADDR_ANY);

		if (bind(RecvSocket, (SOCKADDR *) & RecvAddr, sizeof (RecvAddr))) {
		    printf("bind failed with error %d\n", WSAGetLastError());
		    break;
		}

		SOCKADDR_IN SenderAddr = {0};
		int SenderAddrSize = sizeof(SenderAddr);

		char RecvBuf[1024] = {0};
		int BufLen = 1024;

		printf("Receiving datagrams...\n");
		iResult = recvfrom(RecvSocket, RecvBuf, BufLen, 0, (SOCKADDR *) & SenderAddr, &SenderAddrSize);
		if(iResult > 0){
			puts(RecvBuf);
		}
		else if (iResult == SOCKET_ERROR)
		    printf("recvfrom failed with error %d\n", WSAGetLastError());

		printf("Finished receiving. Closing socket.\n");
		if (closesocket(RecvSocket) == SOCKET_ERROR) {
		    printf("closesocket failed with error %d\n", WSAGetLastError());
		    break;
		}
		iResult = EXIT_SUCCESS;
	}while(FALSE);
    WSACleanup();
	system("pause");
    return iResult;
}
