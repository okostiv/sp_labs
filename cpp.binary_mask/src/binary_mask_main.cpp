#include "binary_mask.h"
#include <iostream>
#include <string>
#include <stack>
#include <limits>
using namespace std;

std::ostream& operator << (std::ostream & os, CBinaryMask& cbm){
	typedef CBinaryMask::_uns _uns;
	_uns value = cbm.value();
	bool* pbArray;
	unsigned uSize = sizeof(_uns)*CHAR_BIT;
	pbArray = new bool[uSize];
	memset(pbArray, false, uSize);
	for(int i(0); value; i++){
		pbArray[i] = (value & (_uns)1); 
		value >>= 1;
	}
	while(-1+uSize--){
		os << pbArray[uSize-1];
	}
	delete[] pbArray;
	return os;
}


int main(){
	cout << CBinaryMask(CHAR_MAX)<< endl;

	CBinaryMask bm_0;
	CBinaryMask bm_3(1|2);
	cout << bm_0.toset(bm_3) << endl;
	cout << bm_0.isset(bm_3) << endl;
	cout << bm_0.unset(CBinaryMask::EMask::mask_2) << endl;

	cin >> *new int;
	return 0;
}			 