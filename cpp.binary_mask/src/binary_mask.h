#ifndef _C_BINNARY_MASK_INCLUDED
#define _C_BINNARY_MASK_INCLUDED
class CBinaryMask{
public:
	typedef char _uns;
	enum EMask: _uns{
		mask_0 = 0x00,
		mask_1 = 0x01,
		mask_2 = 0x02,
		mask_4 = 0x04,
		mask_8 = 0x08
	};
private:
	typedef CBinaryMask _cls;
	typedef enum EMask _msk;
private:
	_uns m_value;
public:
	_cls();
	_cls(_cls const&);
	_cls(_msk const&);
	_cls(_uns const&);

	_cls& toset(_cls const&);
	_cls& unset(_cls const&);
	_cls& reset();

	bool  isset(_cls const&)const;
	_uns  value()const;
};
#endif//_C_BINNARY_MASK_INCLUDED