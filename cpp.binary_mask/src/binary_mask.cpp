#include "binary_mask.h"
#define _cls CBinaryMask
_cls::_cls():
	m_value(_msk())
{}
_cls::_cls(_cls const& cr_obj):
	m_value(cr_obj.m_value)
{}
_cls::_cls(_msk const& cr_val):
	m_value(cr_val)
{}
_cls::_cls(_uns const& cr_val):
	m_value(_msk(cr_val))
{}

_cls& _cls::toset(_cls const& cr_obj)
{
	m_value |= cr_obj.m_value;
	return *this;
}
_cls& _cls::unset(_cls const& cr_obj)
{
	m_value &= ~(cr_obj.m_value);
	return *this;
}
_cls& _cls::reset()
{
	return toset(_msk::mask_0);
}
bool  _cls::isset(_cls const& cr_obj) const
{
	return (m_value & cr_obj.m_value);
}

_cls::_uns _cls::value() const
{
	return m_value;
}
#undef _cls