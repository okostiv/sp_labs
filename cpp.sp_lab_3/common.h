#ifndef _COMMON_H_INCLUDED
#define _COMMON_H_INCLUDED
#include <Windows.h>
#include <cstdio>
#include <conio.h>

typedef CONST PSTR CONST /*const string with const data*/ CPSTRC;
CPSTRC gc_psDir = "D:\\GitRepos\\cpp.sp_labs\\cpp.sp_lab_3\\Debug\\";

INT fn_iEscape(CONST INT c_iCode, CONST PSTR CONST c_psMessage="Press any key or whatever.", CONST BOOL c_bGetch=TRUE){
	puts(c_psMessage);
	if(c_bGetch)
		getch();
	return c_iCode;
}


#endif// _COMMON_H_INCLUDED
