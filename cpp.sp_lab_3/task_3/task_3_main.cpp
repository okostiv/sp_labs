#include "common.h"
#include <set>

DWORD WINAPI fn_dwEratosphen(PVOID);

INT main(INT iArgc, PSTR a_psArgv[]){
	CHAR psInput[25];
	do{
		gets(psInput);
		if(strstr(psInput, "start"))
			break;
	}while(TRUE);

	HANDLE hThread = CreateThread(NULL, 0, fn_dwEratosphen, NULL, 0, NULL);
	if(!hThread)
		puts("<thread> creation failed.");
	else
		puts("<thread> creation succeed.");
	
	while(1){
		gets(psInput);
			if(strstr(psInput, "quit"))
				break;
	}
	CloseHandle(hThread);
	return EXIT_SUCCESS;
}
																				
DWORD WINAPI fn_dwEratosphen(PVOID){
	std::set<int> s_iSieve;
	s_iSieve.insert(2);
	for(int iNumber=3;/*forever*/; iNumber++){
		bool bPut = true;
		for(std::set<int>::const_iterator it = s_iSieve.begin(), it_end = s_iSieve.end(); it != it_end; ++it){
			if(!(iNumber%(*it))){
				bPut = false;
				break;
			}
		}
		if(bPut){
			s_iSieve.insert(iNumber);
			printf("%d ", iNumber);
			Sleep(150);
		}
	}
}