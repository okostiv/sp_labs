// task_2
#include "common.h"
#include <cstring>
																																	
CONST UINT gc_uThreadCount = 2;
CONST PSTR CONST gc_psEventName = "MyEventForThreadSyncronization";

DWORD WINAPI fn_dwThreadFun_1(CONST PVOID CONST); // event synchronization
DWORD WINAPI fn_dwThreadFun_2(CONST PVOID CONST);
PVOID CONST  fn_pvInitParam(CONST INT, CONST PSTR CONST);

INT g_iFlag = 0;

INT main(INT iArgc, PSTR a_psArgv[]){
	puts("Task 2.");
	if(a_psArgv[0][0] == '1')
	{
		HANDLE a_hThread[gc_uThreadCount];
		PVOID pvCriticalSection = new CRITICAL_SECTION;
		InitializeCriticalSection((PCRITICAL_SECTION)pvCriticalSection);
		puts("<critical_section> initialized.");
		for(UINT i=0u; i<gc_uThreadCount; i++)
			a_hThread[i] = CreateThread(NULL, 0, fn_dwThreadFun_2, pvCriticalSection, 0, NULL);
		WaitForMultipleObjects(gc_uThreadCount, a_hThread, TRUE, INFINITE);
		puts("<task_2> suceed.");
		return fn_iEscape(EXIT_SUCCESS);
	} 
	else
	if(a_psArgv[0][0] == '2'){
		HANDLE a_hThread[gc_uThreadCount];
		PVOID a_pvParam[gc_uThreadCount];
		// event creation
		HANDLE hEvent = CreateEvent(NULL, TRUE, FALSE, gc_psEventName);
		/*  � <event> � ��� ����� - <signaled> i <unsignaled> (3-� ��������).
			WaitForSingleObject(<event>) ��������, ���� ���� ���� <signaled>
			SetEvent(<event>) ���������� ���� <signaled>
			ResetEvent(<event>) ���������� ���� <unsignaled> */
		if(!hEvent){
			puts("<event> creation failed.");
			return fn_iEscape(EXIT_FAILURE);
		}else
			puts("<event> creation succeed.");
		// creating threads
		for(INT i=0; i<gc_uThreadCount; i++){
			a_pvParam[i] = fn_pvInitParam(i, gc_psEventName); // initialize params
			a_hThread[i] = CreateThread(NULL, 0, fn_dwThreadFun_1, a_pvParam[i], CREATE_SUSPENDED, NULL); 
		}
		getch();
		for(INT i=0; i<gc_uThreadCount; i++)
			ResumeThread(a_hThread[i]);
		SetEvent(hEvent);
		WaitForMultipleObjects(gc_uThreadCount, a_hThread, TRUE, INFINITE);
		return fn_iEscape(EXIT_SUCCESS);
	}
	else
		return fn_iEscape(EXIT_FAILURE);
}

PVOID CONST fn_pvInitParam(CONST INT c_iNumber, CONST PSTR CONST c_psSentence){
	PVOID pvRat, pvMovable; 
	UINT uMemSize = sizeof(INT) + sizeof(CHAR) * (strlen(c_psSentence) + 1);
	pvRat = pvMovable = malloc(uMemSize); // const and mutable
	if(!pvRat){
		puts("Memory allocation error.");
		return NULL;
	}
	ZeroMemory(pvRat, uMemSize);
	memcpy(pvMovable, &c_iNumber, sizeof(INT));	
	pvMovable = static_cast<PINT>(pvMovable) + 1;
	strcpy((PCHAR)pvMovable, c_psSentence);
	return pvRat;
} 
DWORD WINAPI fn_dwThreadFun_1(CONST PVOID CONST pvParam){
	INT iNumber= *static_cast<PINT>(pvParam);
	printf("<thread_%d> started.\n", iNumber);
	PSTR psSentence = (PSTR)(static_cast<PINT>(pvParam)+1);
	HANDLE hEvent = OpenEvent(EVENT_ALL_ACCESS, FALSE, psSentence);
	if(!hEvent){
		printf("<thread_%d>: <event> opening failed.\n", iNumber);
		return EXIT_FAILURE;
	}else
		printf("<thread_%d>: <event> opening succeed.\n", iNumber);
	DWORD dwWaitForEventResult = WaitForSingleObject(hEvent, INFINITE);
	ResetEvent(hEvent);
	switch(dwWaitForEventResult){
		case WAIT_OBJECT_0:
			for(INT i=0; i<100; i++)
				printf("%d ", (iNumber? -i:i) );
			printf("<thread_%d> suceed.\n", iNumber);
			SetEvent(hEvent);
			CloseHandle(hEvent);
			return EXIT_SUCCESS;
		case WAIT_FAILED:
			printf("<thread_%d> failed.\n", iNumber);
			return EXIT_FAILURE;
	}
}
DWORD WINAPI fn_dwThreadFun_2(CONST PVOID CONST pvParams){
	EnterCriticalSection((PCRITICAL_SECTION)pvParams);
	INT iFlag = g_iFlag++;
	printf("<thread_function> started %d time.\n", g_iFlag);
	for(INT i=0; i<100; i++)
		printf("%d ", iFlag?i:-i);
	LeaveCriticalSection((PCRITICAL_SECTION)pvParams);
	printf("\n<thread_function> %d suceed.\n", g_iFlag);
	return EXIT_SUCCESS;
}
