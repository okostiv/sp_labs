#include "common.h"
#include <cstring>

INT fn_iTaskLuncher(CPSTRC, CPSTRC, CPSTRC); 

INT main(VOID){
	puts("<lab_3> united luncher.");
	CHAR cChoice;
	do{
		puts("Make your choise: [0,1,2,3]");
		cChoice = getchar(); getchar();
		switch(cChoice){
			case '0':
				return fn_iEscape(EXIT_SUCCESS);
			case '1':
				fn_iTaskLuncher(gc_psDir, "task_1.exe", NULL);
				break;
			case '2':
				fn_iTaskLuncher(gc_psDir, "task_2.exe", "1");
				break;
			case '3':
				fn_iTaskLuncher(gc_psDir, "task_2.exe", "2");
			case '4':
				fn_iTaskLuncher(gc_psDir, "task_3.exe", NULL);
		}
	}while(TRUE);
	return (EXIT_SUCCESS);
}// main

INT fn_iTaskLuncher(CPSTRC c_psDir, CPSTRC c_psExe, CPSTRC c_psPrompt){
	STARTUPINFO si;
	PROCESS_INFORMATION pi;
	ZeroMemory(&si, sizeof(si));
	ZeroMemory(&pi, sizeof(pi));

	/* old school stringing*/
	size_t uTargetSize = sizeof(char) * (strlen(c_psDir) + strlen(c_psExe) + 1);
	PSTR psTarget = (PSTR)malloc(uTargetSize);
	ZeroMemory(psTarget, uTargetSize);
	strcat(psTarget, c_psDir);
	strcat(psTarget, c_psExe);

	/* processcreation-waiting stuff */ 
	BOOL bCreationResult = CreateProcess(psTarget, c_psPrompt, NULL, NULL, FALSE, CREATE_NEW_CONSOLE, NULL, NULL, &si, &pi);
	if(bCreationResult){
		printf("%s creation suceed.\n", c_psExe);
		DWORD dwWaitResult = WaitForSingleObject(pi.hProcess, INFINITE);
		switch(dwWaitResult){
			case WAIT_OBJECT_0:
				printf("<%s> suceed.\n", c_psExe);
				return fn_iEscape(EXIT_SUCCESS);
			case WAIT_FAILED:
				printf("<%s> failed.\n", c_psExe);
				return fn_iEscape(EXIT_FAILURE);
		}
	}
	else{
		printf("<%s> creation failed\n", c_psExe);
		return fn_iEscape(EXIT_FAILURE);
	}
}// TaskLuncher