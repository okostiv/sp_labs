#include "common.h"

CONST UINT gc_uThreadCount = 2u; // gc_ == global const

DWORD WINAPI fn_dwThreadFunction_1(PVOID pvParams);
DWORD WINAPI fn_dwThreadFunction_2(PVOID pvParams);

INT main(VOID){
	puts("Task 1.");
	HANDLE ahThread[gc_uThreadCount]; 
	DWORD adwThreadId[gc_uThreadCount];	
	DWORD (WINAPI *afn_dwThreadFunction[2])(PVOID) = {fn_dwThreadFunction_1, fn_dwThreadFunction_2}; // function* array
	PVOID apvThreadFunctionParameter[gc_uThreadCount] = {new INT(500), new INT(500)};

	for(UINT i=0u; i< gc_uThreadCount; i++)
		ahThread[i] = CreateThread(NULL, 100, afn_dwThreadFunction[i], apvThreadFunctionParameter[i], NULL, &adwThreadId[i]);
	SetThreadPriority(ahThread[0], ABOVE_NORMAL_PRIORITY_CLASS);
	SetThreadPriority(ahThread[1], BELOW_NORMAL_PRIORITY_CLASS);

	DWORD dwWaitResult = WaitForMultipleObjects(gc_uThreadCount, ahThread, TRUE, INFINITE);
	switch(dwWaitResult){
		case WAIT_OBJECT_0:
			return fn_iEscape(EXIT_SUCCESS);
		case WAIT_FAILED:
			return fn_iEscape(EXIT_FAILURE);
	}
}

DWORD WINAPI fn_dwThreadFunction_1(PVOID pvParams){
	INT piParam = *(PINT)(pvParams);
	for (INT i=0; i<piParam; i++)
		printf("%d ", i);
	return EXIT_SUCCESS;
}
DWORD WINAPI fn_dwThreadFunction_2(PVOID pvParams){
	INT piParam = *(PINT)pvParams;
	for (INT i=0; i<piParam; i++)
		printf("-%d ", i);
	return EXIT_SUCCESS;
}