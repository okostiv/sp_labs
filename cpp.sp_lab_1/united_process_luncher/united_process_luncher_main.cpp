#include <Windows.h>
#include <cstdio>
#include <conio.h>

#include "../common.h"
VOID fnShowHelp();
BOOL fSecondProcessLuncher(); // opens <first>'s core objects
BOOL fThirdProcessLuncher (); // <first>'s semawhore performance
BOOL fFourthProcessLuncher(); // there's only one current instance possible
BOOL fFifthProcessLuncher (); // inherits parrent core objects

int main(){
	fnShowHelp();
	do{
		CHAR lpsProcessName[25];
		BOOL (*pfnChoisedAction)(void) = NULL;
		puts("Make your choice.");
		CHAR cChoice = getchar();// getchar(); // cvar + \n
		switch(cChoice){
			case '\n':
				continue;
			case '0':
			case 'q':
			case 'e':
				return EXIT_SUCCESS;
			case '1':
			case 'h':
				fnShowHelp();
				break;
			case '2':
				strcpy(lpsProcessName, "<second_process>");
				pfnChoisedAction = fSecondProcessLuncher;
				break;
			case '3':
				strcpy(lpsProcessName, "<third_process>");
				pfnChoisedAction = fThirdProcessLuncher;
				break;
			case '4':
				strcpy(lpsProcessName, "<fourth_process>");
				pfnChoisedAction = fFourthProcessLuncher;
				break;
			case '5':
				strcpy(lpsProcessName, "<fifth_process>");
				pfnChoisedAction = fFifthProcessLuncher;
				break;
			default:
				puts("Type <1> or <h> for help.");
				continue;
		} // switch(cChoise)
		printf("Press any key for %s to start.\n", lpsProcessName);
		_getch();
		BOOL bResult = pfnChoisedAction();
		if(bResult)
			printf("%s succeed.\n", lpsProcessName);
		else
			printf("%s faled.\n", lpsProcessName);
	}while(true);
} // main

VOID fnShowHelp(){
	puts("Help information.");
	puts("\t0 -> exit of.");
	puts("\t1 -> show help.");
	puts("\t2 -> run second task.");
	puts("\t3 -> run third task.");
	puts("\t4 -> run fourth task.");
	puts("\t5 -> run fifth task.");
	return;
}
BOOL fSecondProcessLuncher(){
	STARTUPINFO si;
	PROCESS_INFORMATION piFirstProcess;
	ZeroMemory(&si, sizeof(si));
	ZeroMemory(&piFirstProcess, sizeof(piFirstProcess));
	PSTR psTarget = lpsDir "first_process.exe";
	PSTR psPrompt = "2"; // open: first->second
	BOOL bCreationResult = CreateProcess(psTarget, psPrompt, NULL, NULL, FALSE, CREATE_NEW_CONSOLE, NULL, NULL, &si, &piFirstProcess);
	if(!bCreationResult){
		puts("<second_process> creation failed.");
		return FALSE;
	}
	else
		puts("<second_process> creation succeed.");
	WaitForSingleObject(piFirstProcess.hProcess, INFINITE);
	return TRUE;
} // fSecondProcessLuncher
BOOL fThirdProcessLuncher(){
	HANDLE ahProcesses[3] = {0,0,0};
	INT iCreationCount(0);
	for(INT i(0); i<3; i++){
		PROCESS_INFORMATION piThird;
		ZeroMemory(&piThird, sizeof(piThird));
		STARTUPINFO si;
		ZeroMemory(&si, sizeof(si));
		LPSTR lpsTarget = lpsDir "third_process.exe";
		LPSTR lpsPrompt = "TheNameOfMyVeryOwnSemaphoreObjectForThirdProcessesToMakeSomeTrickyPerformances";
		BOOL bCreationResult = CreateProcess(lpsTarget, lpsPrompt, NULL, NULL, FALSE, CREATE_NEW_CONSOLE, NULL, NULL, &si, &piThird);
		if(bCreationResult){
			printf("<third_process> creation succeed %d time.\n", i+1);
			ahProcesses[iCreationCount++] = piThird.hProcess;
		}
		else
			printf("<third_process> creation failed %d time.\n", i+1);
	}
	WaitForMultipleObjects(iCreationCount, ahProcesses, TRUE, INFINITE);
	return TRUE;
} // fThirdProcessLuncher
BOOL fFourthProcessLuncher(){
	STARTUPINFO si;
	PROCESS_INFORMATION piFourthProcess;
	ZeroMemory(&si, sizeof(si));
	ZeroMemory(&piFourthProcess, sizeof(piFourthProcess));
	LPSTR lpsTarget = lpsDir "fourth_process.exe";
	BOOL bCreationResult = CreateProcess(lpsTarget, NULL, NULL, NULL, FALSE, CREATE_NEW_CONSOLE, NULL, NULL, &si, &piFourthProcess);
	if(!bCreationResult){
		puts("<fourth_process> creation failed.");
		return FALSE;
	}
	else
		puts("<fourth_process> creation succeed.");
	WaitForSingleObject(piFourthProcess.hProcess, INFINITE);
	return TRUE;
} // fFourthProcessLuncher
BOOL fFifthProcessLuncher(){
	STARTUPINFO si;
	PROCESS_INFORMATION piFirstProcess;
	ZeroMemory(&si, sizeof(si));
	ZeroMemory(&piFirstProcess, sizeof(piFirstProcess));
	PSTR psTarget = lpsDir "first_process.exe";
	PSTR psPrompt = "5"; // open: first->fifth
	BOOL bCreationResult = CreateProcess(psTarget, psPrompt, NULL, NULL, FALSE, CREATE_NEW_CONSOLE, NULL, NULL, &si, &piFirstProcess);
	if(!bCreationResult){
		puts("<second_process> creation failed.");
		return FALSE;
	}
	else
		puts("<second_process> creation succeed.");
	WaitForSingleObject(piFirstProcess.hProcess, INFINITE);
	return TRUE;
} // fFifthProcessLuncher