#include <cstdio>
#include <conio.h>
#include <Windows.h>

#include "../common.h"

int main(INT argc, LPSTR argv[]){
	puts("Process 2");

	HANDLE hMutex = CreateMutex(NULL, TRUE, argv[0]);
	if(!hMutex)
		puts("Mutex creation error.");
	else 
		if(GetLastError()==ERROR_ALREADY_EXISTS)
			puts("Mutex already existed.");

	DWORD dwWaitResult = WaitForSingleObject(hMutex, INFINITE);
	switch(dwWaitResult){
		case WAIT_OBJECT_0:
			puts("Process obtained mutex. Now you should release it.");
			break;
		case WAIT_FAILED:
			puts("Process failed to obtain mutex.");
	}

	puts("Press any key or whatever.");
	getch();
	CloseHandle(hMutex);
	return EXIT_SUCCESS;
}