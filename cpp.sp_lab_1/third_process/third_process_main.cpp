#include <Windows.h>
#include <cstdio>
#include <conio.h>

#include "../common.h"

INT main(INT iArgc, LPSTR lpsArgv[]){
	puts("Process 3.");
	HANDLE hSemaphore = CreateSemaphore(NULL, 2, 2, lpsArgv[0]);
	if(!hSemaphore){
		puts("Creation (opening) of the semaphore has failed.");
		return EXIT_FAILURE;
	}
	else{
		puts("Creation (opening) of the semaphore has succeed.");
	}

	do{
		DWORD dwWaitResult = WaitForSingleObject(hSemaphore, 0);
		switch(dwWaitResult){
			case WAIT_OBJECT_0:
				puts("Process's taken control over the semaphore.");
				puts("Press any key to release it.");
				getch();
				ReleaseSemaphore(hSemaphore, 1, nullptr);
				CloseHandle(hSemaphore);
				return EXIT_SUCCESS;
			case WAIT_TIMEOUT:
				puts("Process is not aple to take over the semaphore yet. Some other processes must release object.");
				getch();
				break;
			case WAIT_FAILED:
				puts("Something's wrong. N I just donno what to do.");
				getch();
				break;
		}
	}while(true);
}
