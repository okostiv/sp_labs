#include <cstdio>
#include <conio.h>
#include <cstdlib>
#include <Windows.h>
#include <cwchar>

#include <iostream>
using namespace std;

#include "../common.h" // some helpful crap Ich made
INT fnFirstToSecond();
INT fnFirstToFifth();

INT main(INT iArgc, PSTR psArgv[]){
	puts("Process 1");
	INT iResult;
	CHAR cPerformanceVariant = psArgv[0][0]; // determines "behavior" of <first_process> (goto 2 next comments)
	if(cPerformanceVariant == '2'){
		puts("Part 1->2.");
		iResult = fnFirstToSecond(); // <first_process> calls <second_process>
	}
	if(cPerformanceVariant == '5'){
		puts("Part 1->5.");
		iResult = fnFirstToFifth(); // <first_process> calls <fifth_process>
	}
	return iResult;
}// main
INT fnFirstToSecond(){
	puts("Part 2.");
		HANDLE hMutex = CreateMutex(NULL, FALSE, "NameOfMutex");

		STARTUPINFO si;
		PROCESS_INFORMATION piSecond;
		ZeroMemory(&si, sizeof(si));
		ZeroMemory(&piSecond, sizeof(piSecond));

		LPSTR lpsTarget = lpsDir "second_process.exe";
		LPSTR lpsPrompt = "NameOfMutex";

		BOOL bCreationSecondSuccess = CreateProcess(lpsTarget, lpsPrompt, NULL, NULL, FALSE, CREATE_NEW_CONSOLE|HIGH_PRIORITY_CLASS, NULL, NULL, &si, &piSecond);
		if(!bCreationSecondSuccess){
			puts("<second_process> failed.");
			return EXIT_FAILURE;
		}
		ReleaseMutex(hMutex);
		puts("Mutex was released. Press any key to start waiting for it.");
		getch(); // actually that is for thread to fall asleep for a while
		do{
			DWORD dwWaitForSecondResult = WaitForSingleObject(hMutex, 2000);
			switch(dwWaitForSecondResult){
					case WAIT_OBJECT_0:
						puts("<second_process> succeed.");
						CloseHandle(hMutex);
						return fnMyExit(EXIT_SUCCESS);
					case WAIT_TIMEOUT:
						puts("<second_process> is holding mutex.");
						break;
					case WAIT_FAILED:
						puts("Something went wrong.");
						return fnMyExit(EXIT_FAILURE);
			}
		}while(TRUE);
}// fnFirstToSecond
INT fnFirstToFifth(){
	SECURITY_ATTRIBUTES siInheritable;
	ZeroMemory(&siInheritable, sizeof(siInheritable));
	siInheritable.bInheritHandle = TRUE;

	HANDLE hTimer = CreateWaitableTimer(&siInheritable, TRUE, NULL); // nameless inheritable timer
	if(!hTimer){
		puts("Timer creation failed.");
		return fnMyExit(EXIT_FAILURE);
	}
	else
		puts("Timer creation succeed.");
	LARGE_INTEGER liDueTime;
	liDueTime.QuadPart = 100*10000000LL;
	puts("Press any key to release timer.");
	getch();
	SetWaitableTimer(hTimer, &liDueTime, 0, NULL, NULL, TRUE);
	puts("Timer was released.");

	STARTUPINFO si;
	PROCESS_INFORMATION piFifth;
	ZeroMemory(&si, sizeof(si));
	ZeroMemory(&piFifth, sizeof(piFifth));

	PSTR psTarget = lpsDir"fifth_process.exe";
	CHAR psPrompt[25];
	ZeroMemory(psPrompt, sizeof(char)*25);
	//strcpy(psPrompt, htos(hTimer));
	sprintf(psPrompt, "%d", (INT)hTimer);
	cout << psPrompt << endl;
	BOOL bCreationResult = CreateProcess(psTarget, psPrompt, NULL, NULL, TRUE, CREATE_NEW_CONSOLE, NULL, NULL, &si, &piFifth);
	if(!bCreationResult){
		puts("<fifth_process> failed.");
		return fnMyExit(EXIT_FAILURE);
	}
	
	WaitForSingleObject(piFifth.hProcess, INFINITE);
	puts("<fifth_process> suceed.");
	return fnMyExit(EXIT_SUCCESS);

}// fnFirstToFifth