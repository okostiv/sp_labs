#include <Windows.h>
#include <cstring>
#include <cstdlib>

#define lpsDir "D:\\GitRepos\\cpp.sp_labs\\cpp.sp_lab_1\\Debug\\"
#define TEXT(x) #x

PSTR htos(HANDLE hObject){
	CHAR psBuffer[25];
	ZeroMemory(psBuffer, sizeof(CHAR)*25);
	_ultoa((ULONG)hObject, psBuffer, 10);
	return psBuffer;
}
HANDLE stoh(LPSTR lpsValueBuffer){
	LPSTR lpsStopString = "STP";
	strcat(lpsValueBuffer, lpsStopString);
	ULONG ulVal = strtoul(lpsValueBuffer, &lpsStopString, 10); 
	return (HANDLE)ulVal;
}
LPSTR supercat(USHORT quant, LPSTR lpsArg1, ...){
	LPSTR* args = &lpsArg1;
	UINT length(0);
	for(USHORT i(0); i<quant; i++)
		length += strlen(args[i]) + strlen(" ");
	LPSTR lpsRat = new CHAR[length + 1];
	lpsRat[0] = char(NULL);
	for(USHORT i(0); i<quant; i++){
		strcat(lpsRat, args[i]);
		strcat(lpsRat, " ");
	}
	return lpsRat;
}

INT fnMyExit(INT iCode, PSTR psMessage = "Press any key or whatever.", BOOL bGetch = TRUE){
	puts(psMessage);
	if(bGetch)
		getch();
	return iCode;
}
