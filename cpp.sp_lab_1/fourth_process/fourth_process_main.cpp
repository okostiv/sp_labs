#include <Windows.h>
#include <cstdio>
#include <conio.h>

int main(INT argc, LPSTR argv[]){
	puts("Process 4");
	HANDLE hNamedMutex = CreateMutex(NULL, TRUE, "NamedMutexOfMine");
	if(GetLastError() == ERROR_ALREADY_EXISTS){
		puts("This program has already started up");
	}
	else{
		puts("Program has started at once.");
		puts("Recall? (y/n)");
		CHAR cChoise = getchar();
		if(cChoise == 'y'){
			PROCESS_INFORMATION piSelfCreating;
			ZeroMemory(&piSelfCreating, sizeof(piSelfCreating));
			STARTUPINFO si;
			ZeroMemory(&si, sizeof(si));
			LPSTR lpsTarget = "D:\\GitRepos\\cpp.sp_labs\\cpp.sp_lab_1\\Debug\\fourth_process.exe";
			BOOL bCreationResult = CreateProcess(lpsTarget, NULL, NULL, NULL, FALSE, CREATE_NEW_CONSOLE, NULL, NULL, &si, &piSelfCreating);
			if(!bCreationResult){
				puts("Sorry, some problems appeared.");
			}
		}
	}
	puts("Press any key or whatever.");
	_getch();
	CloseHandle(hNamedMutex);
	return EXIT_SUCCESS;
}// main