#include <Windows.h>
#include <cstdio>
#include <conio.h>

#include "../common.h"

INT main(INT iArgc, PSTR psArgv[]){
	puts("Process 5.");
	HANDLE hInheritedTimer = NULL;// = stoh(psArgv[0]);
	sscanf(psArgv[0],"%d", (INT*)&hInheritedTimer);
	DWORD dwWaitResult = WaitForSingleObject(hInheritedTimer, INFINITE); 
	switch(dwWaitResult){
		case WAIT_OBJECT_0:
			puts("Process obtained Timer.");
			CloseHandle(hInheritedTimer);
			return fnMyExit(EXIT_SUCCESS);
		case WAIT_FAILED:
			puts("Process failed to obtain timer.");
			return fnMyExit(EXIT_FAILURE);
	}
	return EXIT_SUCCESS;
}